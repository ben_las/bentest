Question 1:
command : git status
answer: On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   hello_world.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Ex2-Answers_file.txt

command : git diff
answer:
diff --git a/hello_world.txt b/hello_world.txt
index 5627a75..84931ac 100644
--- a/hello_world.txt
+++ b/hello_world.txt
@@ -4,6 +4,6 @@ Aenean commodo ligula eget dolor.
 Aenean massa.
 Cum sociis natoque penatibus et magnis dis parturient montes,
 nascetur ridiculus mus.
-Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
+Donec quam felis, ultricies, pellentesque eu, pretium quis, sem.^M
 Nulla consequat massa quis enim.
 Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
\ No newline at end of file

Question 2:

before the reset :

command : git status:
answer:
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   hello_world.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Ex2-Answers_file.txt

command : git reset --hard
answer : HEAD is now at 12529cb 'Ex1' 

command : git status
answer:
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Ex2-Answers_file.txt

Question 3:

command : git add hello_world.txt
command : git commit -m 'ex2_q3'
answer:
[master abfc07b] 'ex2_q3'
 1 file changed, 1 insertion(+), 1 deletion(-)

command : git log
answer : 
commit abfc07b91745d6ae280835a40b796f92ae30e846 (HEAD -> master)
Author: benjamin_lastmann <lastmannbenjamin@gmail.com>
Date:   Wed Oct 17 12:29:41 2018 +0300

    'ex2_q3'

commit 12529cbcf9794627eefb36ca98ab7cc1a385673b
Author: benjamin_lastmann <lastmannbenjamin@gmail.com>
Date:   Wed Oct 17 12:11:28 2018 +0300

    'Ex1'

commit fbf4df0f68dd81c80f786b880a4724120a22594d
Author: benjamin_lastmann <lastmannbenjamin@gmail.com>
Date:   Wed Oct 17 11:59:11 2018 +0300

    'first_try_of_changes'

command : 
git checkout 12529cbcf9794627eefb36ca98ab7cc1a385673b
answer: 
Previous HEAD position was abfc07b 'ex2_q3'
HEAD is now at 12529cb 'Ex1'

command : git status
answer:
HEAD detached at 12529cb
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Ex2-Answers_file.txt

command : 
git diff abfc07b91745d6ae280835a40b796f92ae30e846
answer :
diff --git a/hello_world.txt b/hello_world.txt
index a4aea34..5627a75 100644
--- a/hello_world.txt
+++ b/hello_world.txt
@@ -5,5 +5,5 @@ Aenean massa.
 Cum sociis natoque penatibus et magnis dis parturient montes,
 nascetur ridiculus mus.
 Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
-Nulla consequat massa quis enim testtoseeifitworks.
+Nulla consequat massa quis enim.^M
 Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
\ No newline at end of file

command :
git reflog
answer :
12529cb (HEAD) HEAD@{0}: checkout: moving from abfc07b91745d6ae280835a40b796f92ae30e846 to 12529cbcf9794627eefb36ca98ab7cc1a385673b
abfc07b (master) HEAD@{1}: checkout: moving from 12529cbcf9794627eefb36ca98ab7cc1a385673b to abfc07b91745d6ae280835a40b796f92ae30e846
12529cb (HEAD) HEAD@{2}: checkout: moving from master to HEAD~1
abfc07b (master) HEAD@{3}: commit: 'ex2_q3'
12529cb (HEAD) HEAD@{4}: reset: moving to HEAD
12529cb (HEAD) HEAD@{5}: commit: 'Ex1'
fbf4df0 HEAD@{6}: commit (initial): 'first_try_of_changes'

command : git checkout abfc07b
answer :
Previous HEAD position was 12529cb 'Ex1'
HEAD is now at abfc07b 'ex2_q3'

command : git status
answer:
HEAD detached at abfc07b
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Ex2-Answers_file.txt

